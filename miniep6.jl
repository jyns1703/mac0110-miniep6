# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    i = 0
    a = 1
    while i <  n
      a = a + i
      i = i + 1
    end
    inicio = 2 * a - 1
    return inicio
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    i = 0
    a = 1
    while i < m
      a = a + i
      i = i + 1
    end
    inicio = 2 * a - 1
    print(m," ")
    pot = m * m * m
    print(pot," ")
    j = 1
    a = inicio
    while j <= m
      print(a," ")
      a = a + 2
      j = j + 1
    end
end


function mostra_n(n)
   for x in 1:n
     imprime_impares_consecutivos(x)
     println(" ")
   end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
